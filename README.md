# README #

You have to implement an app in Java/Kotlin using Android SDK for the Mavel Api.

### What do you need to start? ###

* Android Studio with Android SDK
* An Account on [Marvel](http://developer.marvel.com/) and get your developer key

### What do you have to do? ###

* Implement a list of heroes like this [Heroes list](https://bytebucket.org/omitsis-team/android-test/raw/04a77edf723432f1a43af07d3dd0e8922ef4cb5b/screenshots/h_list.png?token=fd8556d26da24e220e56ccdbe87d14a5a7e3f192)
 + Search by heroe name must be available
 
* You can navigate to see detail of the heroe when you tap on a item of the list

* The detail view show the info of the heroe and their related comics and events: [Hero detail](https://bytebucket.org/omitsis-team/android-test/raw/04a77edf723432f1a43af07d3dd0e8922ef4cb5b/screenshots/h_detail.png?token=b91605d32d765c324c031330736ad2bd17287675)
 + In the tabs, the total of Events or Comics that the heroes have should be shown

### What to do when you finish? ###

* Push the code on a private repo and give to this email [android@omitsis.com](android@omitsis.com) permissions to get your code 
* Send us an email with link to your private report



# Heroes List #
![Heroes list](https://bytebucket.org/omitsis-team/android-test/raw/04a77edf723432f1a43af07d3dd0e8922ef4cb5b/screenshots/h_list.png?token=fd8556d26da24e220e56ccdbe87d14a5a7e3f192)

# Hero Detail #
![Hero detail](https://bytebucket.org/omitsis-team/android-test/raw/04a77edf723432f1a43af07d3dd0e8922ef4cb5b/screenshots/h_detail.png?token=b91605d32d765c324c031330736ad2bd17287675)